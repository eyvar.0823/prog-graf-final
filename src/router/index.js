import VueRouter from 'vue-router'
import Vue from 'vue'
import ProductDetails from './../components/ProductDetails.vue'
import Home from './../components/Home.vue'
import LoginComponent from "./../components/login.vue"
import SecureComponent from "./../components/secure.vue"

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/',
      redirect: {
          name: "home"
      }
  },
  {
      path: "/login",
      name: "login",
      component: LoginComponent
  },
  {
      path: "/secure",
      name: "secure",
      component: SecureComponent
  },
    {path: '/products/:id',
     component: ProductDetails
    },
    {path: '/',
     name: "home",
     component: Home
    },
  ],
})
