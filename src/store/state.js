export const state = {
  products: [
    {
      id: 1,
      name: 'Assassins Creed Odissey',
      price: 40.00,
      description: 'Assassins Creed Odissey',
      image: 'dist/images/products/AC_Odissey.jpeg',
    },
    {
      id: 2,
      name: 'Assassins Creed Black Flag',
      price: 19.99,
      description: 'Assassins Creed Black Flag',
      image: 'dist/images/products/Black_Flag.jpeg',
    },
    {
      id: 3,
      name: 'Call of Duty Black Ops 4',
      price: 37.50,
      description: 'Call of Duty Black Ops 4',
      image: 'dist/images/products/BO4.jpeg',
    },
    {
      id: 4,
      name: 'Dishonored',
      price: 7.31,
      description: 'Dishonored',
      image: 'dist/images/products/Dishonored.jpeg',
    },
    {
      id: 5,
      name: 'Farcry 5',
      price: 30.00,
      description: 'Farcry 5',
      image: 'dist/images/products/Farcry5.jpeg',
    },
  ],
}
